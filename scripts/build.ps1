"Building the debug version...";
node node_modules/uglify-es/bin/uglifyjs sources/base.js sources/contract.js sources/dom.js sources/dom.attributes.js sources/managed.enum.js --source-map 'url="dbc.debug.js.map",includeSources="true"' -b -o build/dbc.debug.js;

"Building the release version...";
node node_modules/uglify-es/bin/uglifyjs ./sources/base.js ./sources/contract.js ./sources/dom.js ./sources/dom.attributes.js ./sources/managed.enum.js -c -m -o ./build/dbc.js