#!/bin/bash

printf "Setting the permissions for scripts & sources...\n";
chmod +x ./scripts/build.project.sh
chmod -R 0777 ./sources

printf "Started to restore the development dependencies...\n";
output=$(node ./scripts/prepare.deplist.js);
eval "$output";
eval "npm run build";