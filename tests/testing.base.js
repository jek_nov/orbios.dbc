(function( globalContext ) {
    "use strict";

    describe("Base tests", () => {
        it("Testing the library existence in the global scope.", () => {
            expect(typeof globalContext.Dbc.Base === "function").toBe(true);
        });
    });

    describe("Base tests", () => {
        it("Testing the empty string getter.", () => {
            expect(typeof globalContext.Dbc.Base.emptyString === "string").toBe(true);
            expect(globalContext.Dbc.Base.emptyString === "").toBe(true);
        });
    });

    describe("Base tests", () => {
        it("Testing the equality method.", () => {
            const firstItem  = 3;
            const secondItem = 3;

            expect(Dbc.Base.equal(firstItem, secondItem)).toBe(true);
        });
    });

    describe("Base tests", () => {
        it("Testing the assertion method.", () => {
            const state = true;

            expect(Dbc.Base.assert({
                condition: typeof state === "boolean"
            })).toBe(true);

            expect(Dbc.Base.assert({
                condition: globalContext instanceof Window
            })).toBe(true);
        });
    });

    describe("Base tests", () => {
        it("Testing the delay method for the timer id.", () => {
            const timerId = Dbc.Base.delay(() => {}, 0);
            expect(typeof timerId === "number").toBe(true);
        });
    });

    describe("Base tests", () => {
        it("Testing the capitalize method.", () => {
            const value = "moscow";
            const expectedResult = "Moscow";

            expect(Dbc.Base.capitalize(value) === expectedResult).toBe(true);
        });
    });

    describe("Base tests", () => {
        it("Testing the raise method.", () => {
            let hasThrown = false;

            try {
                Dbc.Base.raise("Throwing some exception.");
            }
            catch(exception) {
                hasThrown = true;
            }

            expect(hasThrown).toBe(true);
        });
    });

    describe("Base tests", () => {
        it("Testing the immute method.", () => {
            let hasThrown  = false;
            const instance = { foo: 1, bar: 2 };
            Dbc.Base.immute(instance);

            try {
                delete instance.foo;
            }
            catch(exception) {
                hasThrown = true;
            }

            expect(hasThrown).toBe(true);
        });
    });

})( this );