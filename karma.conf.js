module.exports = config => {
    config.set({
        basePath      : "",
        frameworks    : [ "jasmine" ],
        exclude       : [],
        preprocessors : {},
        reporters     : [ "progress" ],
        port          : 9876,
        colors        : true,
        logLevel      : config.LOG_INFO,
        autoWatch     : true,
        singleRun     : true,
        concurrency   : Infinity,
        browsers      : [ "ChromeHeadlessNoSandbox", "Firefox" ],
        customLaunchers: {
            ChromeHeadlessNoSandbox: {
                base: 'ChromeHeadless',
                flags: [
                    '--no-sandbox',
                    '--user-data-dir=/tmp/chrome-test-profile',
                    '--disable-web-security'
                ]
            }
        },
        plugins       : [
            "karma-jasmine",
            "karma-chrome-launcher",
            "karma-firefox-launcher"
        ],
        files: [
            "./build/dbc.debug.js",
            "./tests/testing.base.js",
            "./tests/testing.contract.js",
            "./tests/testing.enum.js",
            "./tests/testing.dom.js",
            "./tests/testing.dom.attributes.js",
            { pattern: "./tests/assets/script.js"     , included: false },
            { pattern: "./tests/assets/style.css"     , included: false },
            { pattern: "./tests/assets/template.html" , included: false },
            { pattern: "./tests/assets/copies.html"   , included: false },
            { pattern: "./tests/assets/note.txt"      , included: false }
        ]
    });
};