const gulp = require('gulp');
let exec = require('child_process').exec,child;

gulp.task('build', async function() {
    const systemType = process.platform;
    console.log(`Current OS: ${systemType}`);

    switch(systemType) {
        case "win32":
            console.log("Starting PS script...");
            try {
                await prepareFolderWindows();
                const message = await buildWindowsSystem();
                console.log(message);
            }
            catch(exception) {
                console.log(exception);
            }            
            break;
        case "linux":
            console.log("Starting bash script...");
            try {
                await prepareFolderLinux();
                const result = await buildLinuxSystem();
                console.log(result);
            }
            catch(exception) {
                console.log(exception);
            }            
            break;
        default:
            console.log("Error: Unsupported operation system.");
            process.exit(0);
            break;
    }    
});

gulp.task('mbk', callback => {
    const systemType = process.platform;
    console.log(`Current OS: ${systemType}`);

    switch(systemType) {
        case "win32":
            exec("copy package.json package.backup.json /Y", 
                (error, cmdOutput, cmdError) => callback(error));
            break;
        case "linux":
            exec("cp package.json package.backup.json", 
                (error, cmdOutput, cmdError) => callback(error));      
            break;
        default:
            console.log("Error: Unsupported operation system.");
            process.exit(0);
            break;
    } 
});

gulp.task('clean', callback => {
    const systemType = process.platform;
    console.log(`Current OS: ${systemType}`);

    switch(systemType) {
        case "win32":
            exec("(if exist .\\build rmdir .\\build /s /q) && (if exist .\\node_modules rmdir .\\node_modules /s /q) &&" +
                "(if exist .\\package-lock.json del .\\package-lock.json) && (if exist .\\package.backup.json del .\\package.backup.json)", 
                (error, cmdOutput, cmdError) => callback(error));
            break;
        case "linux":
            exec("rm -rf package-lock.json package.backup.json ./node_modules ./build", 
                (error, cmdOutput, cmdError) => callback(error));      
            break;
        default:
            console.log("Error: Unsupported operation system.");
            process.exit(0);
            break;
    } 
});

gulp.task('postinstall', async function() {
    const systemType = process.platform;
    console.log(`Current OS: ${systemType}`);

    switch(systemType) {
        case "win32":
        try {
            const message = await runPostinstallWindows();
            console.log(message);
        }
        catch(exception) {
            console.log(exception);
        }
        break;
        case "linux":
            const message = await runPostinstallLinux();     
            break;
        default:
            console.log("Error: Unsupported operation system.");
            process.exit(0);
            break;
    }
});

async function buildLinuxSystem() {
    return new Promise((resolve, reject) => {
        child = exec('sh ./scripts/build.project.sh');
        child.stdout.on("data", function(data) {
            console.log("Bash output: " + data);
        });

        child.stderr.on("data", function(data) {
            console.log(`Bash errors: ${data}`);                
            reject(`Error: ${data}`);
        });

        child.on("exit", function() {
            console.log("Bash script finished.");                
            resolve("Building successfully completed.");
        });
        child.stdin.end(); //end input
    });
}

async function buildWindowsSystem() {
    return new Promise((resolve, reject) => {
        child = exec('powershell.exe -executionpolicy remotesigned .\\scripts\\build.ps1');
        child.stdout.on("data", function(data) {
            console.log("Powershell data: " + data);
        });

        child.stderr.on("data", function(data) {
            console.log(`Powershell errors: ${data}`);                
            reject(`Error: ${data}`);
        });

        child.on("exit", function() {
            console.log("Powershell build script finished.");                
            resolve("Building successfully completed.");
        });
        child.stdin.end(); //end input
    });
}

async function runPostinstallWindows() {
    return new Promise((resolve, reject) => {
        child = exec('powershell.exe -executionpolicy remotesigned .\\scripts\\postinstall.ps1');
        child.stdout.on("data", function(data) {
            console.log("Powershell data: " + data);
        });

        child.stderr.on("data", function(data) {
            console.log(`Powershell errors: ${data}`);                
            reject(`Error: ${data}`);
        });

        child.on("exit", function() {
            console.log("Powershell postinstall script finished.");                
            resolve("Postinstall successfully completed.");
        });
        child.stdin.end(); //end input
    });
}

async function runPostinstallLinux() {
    return new Promise((resolve, reject) => {
        child = exec('chmod +x ./scripts/postinstall.sh && ./scripts/postinstall.sh');
        child.stdout.on("data", function(data) {
            console.log("Shell output: " + data);
        });

        child.stderr.on("data", function(data) {
            console.log(`Shell script errors: ${data}`);
        });

        child.on("exit", function() {
            console.log("Bash postinstall script finished.");                
            resolve("Postinstall successfully completed.");
        });
        child.stdin.end(); //end input
    });
}

async function prepareFolderWindows() {
    return new Promise((resolve, reject) => {
        child = exec("(if exist .\\build rmdir .\\build /s /q) && mkdir build");
        child.stderr.on("data", function(data) {
            console.log(`Directory prepare errors: ${data}`);
            reject(`Error: ${data}`);
        });

        child.on("exit", function() {
            console.log("Directory prepared.");
            resolve("Directory prepared.");
        });
        child.stdin.end(); //end input
    });
}

async function prepareFolderLinux() {
    return new Promise((resolve, reject) => {
        child = exec("rm -rf build && mkdir build");
        child.stderr.on("data", function(data) {
            console.log(`Directory prepare errors: ${data}`);
            reject(`Error: ${data}`);
        });

        child.on("exit", function() {
            console.log("Directory prepared.");  
            resolve("Directory prepared.");
        });
        child.stdin.end(); //end input
    });
}